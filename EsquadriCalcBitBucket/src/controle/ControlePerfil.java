/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;


import factory.ConectaBanco;
import visao.FrmConsultaPerfil;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import modelo.ModeloPerfil;

/**
 *
 * @author Plinio
 */
public class ControlePerfil {
    
    ConectaBanco conectaBanco = new ConectaBanco();
    ModeloPerfil modeloPerfil = new ModeloPerfil();

    public ControlePerfil() {
        
    }
    
    public void salvar(ModeloPerfil modeloPerfil){
        conectaBanco.conexao();
        try {
            PreparedStatement pst = conectaBanco.conectar.prepareStatement("insert into perfil(codigo_perfil, descricao_perfil, peso_perfil, comprimento_perfil) values(?,?,?,?)");
            pst.setString(1, modeloPerfil.getCodigo());
            pst.setString(2, modeloPerfil.getDescricao());
            pst.setDouble(3, modeloPerfil.getPeso());
            pst.setDouble(4, modeloPerfil.getComprimento());
            pst.execute();
            JOptionPane.showMessageDialog(null, "Dados cadastrados com Sucesso!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao inserir dados.\nErro: " + ex.getMessage());
        }
        
    }
    
    public void alterar(ModeloPerfil modeloPerfil){
        conectaBanco.conexao();
        
        try {
            PreparedStatement pst = conectaBanco.conectar.prepareStatement("update perfil set codigo_perfil = ?, descricao_perfil = ?, peso_perfil = ?, comprimento_perfil = ? where id_perfil = ?");
            pst.setString(1, modeloPerfil.getCodigo());
            pst.setString(2, modeloPerfil.getDescricao());
            pst.setDouble(3, modeloPerfil.getPeso());
            pst.setDouble(4, modeloPerfil.getComprimento());
            pst.setDouble(5, modeloPerfil.getId());
            
            pst.execute();
            JOptionPane.showMessageDialog(null, "Registro alterado com sucesso!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao alterar dados.\nErro: " + ex.getMessage());
        }
        conectaBanco.desconecta();
    }
    
    public void consulta(ModeloPerfil modeloPerfil){
        conectaBanco.conexao();
                
        
        
        
    }
    
    public void excluir(ModeloPerfil modeloPerfil){
        conectaBanco.conexao();
        
        try {
            PreparedStatement pst = conectaBanco.conectar.prepareStatement("delete from perfil where codigo_perfil = ?");
            pst.setString(1, modeloPerfil.getCodigo());
            pst.execute();
            JOptionPane.showMessageDialog(null, "Registro excluído com Sucesso!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao excluir registro.\nErro: " + ex.getMessage());
        }
        conectaBanco.desconecta();
    }

    public ModeloPerfil primeiro() {
        conectaBanco.conexao();
        try {
            conectaBanco.executaSql("select * from perfil");
            conectaBanco.rs.first();
            modeloPerfil.setId(conectaBanco.rs.getLong("id_perfil"));
            modeloPerfil.setCodigo(conectaBanco.rs.getString("codigo_perfil"));
            modeloPerfil.setDescricao(conectaBanco.rs.getString("descricao_perfil"));
            modeloPerfil.setPeso(conectaBanco.rs.getDouble("peso_perfil"));
            modeloPerfil.setComprimento(conectaBanco.rs.getDouble("comprimento_perfil"));
            modeloPerfil.setLinha(conectaBanco.rs.getString("linha_perfil"));
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao preencher campos.\nErro: " + ex.getMessage());
        }
        conectaBanco.desconecta();
        return modeloPerfil;
    }

    public ModeloPerfil anterior() {
        conectaBanco.conexao();
        try {
            conectaBanco.rs.previous();
            modeloPerfil.setId(conectaBanco.rs.getLong("id_perfil"));
            modeloPerfil.setCodigo(conectaBanco.rs.getString("codigo_perfil"));
            modeloPerfil.setDescricao(conectaBanco.rs.getString("descricao_perfil"));
            modeloPerfil.setPeso(conectaBanco.rs.getDouble("peso_perfil"));
            modeloPerfil.setComprimento(conectaBanco.rs.getDouble("comprimento_perfil"));
            modeloPerfil.setLinha(conectaBanco.rs.getString("linha_perfil"));
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao preencher campos.\nErro: " + ex.getMessage());
        }
        conectaBanco.desconecta();
        return modeloPerfil;
    }

    public ModeloPerfil proximo() {
        conectaBanco.conexao();
        try {
            conectaBanco.rs.next();
            modeloPerfil.setId(conectaBanco.rs.getLong("id_perfil"));
            modeloPerfil.setCodigo(conectaBanco.rs.getString("codigo_perfil"));
            modeloPerfil.setDescricao(conectaBanco.rs.getString("descricao_perfil"));
            modeloPerfil.setPeso(conectaBanco.rs.getDouble("peso_perfil"));
            modeloPerfil.setComprimento(conectaBanco.rs.getDouble("comprimento_perfil"));
            modeloPerfil.setLinha(conectaBanco.rs.getString("linha_perfil"));
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao preencher campos.\nErro: " + ex.getMessage());
        }
        conectaBanco.desconecta();
        return modeloPerfil;
    }

    public ModeloPerfil ultimo() {
        conectaBanco.conexao();
        
        try {
            conectaBanco.executaSql("select * from perfil");
            conectaBanco.rs.last();
            modeloPerfil.setId(conectaBanco.rs.getLong("id_perfil"));
            modeloPerfil.setCodigo(conectaBanco.rs.getString("codigo_perfil"));
            modeloPerfil.setDescricao(conectaBanco.rs.getString("descricao_perfil"));
            modeloPerfil.setPeso(conectaBanco.rs.getDouble("peso_perfil"));
            modeloPerfil.setComprimento(conectaBanco.rs.getDouble("comprimento_perfil"));
            modeloPerfil.setLinha(conectaBanco.rs.getString("linha_perfil"));
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao preencher campos.\nErro: " + ex.getMessage());
        }
        conectaBanco.desconecta();
        return modeloPerfil;
    }
    
}
