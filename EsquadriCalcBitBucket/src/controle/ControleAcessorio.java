/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;

import factory.ConectaBanco;
import visao.FrmConsultaAcessorio;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import modelo.ModeloAcessorio;

/**
 *
 * @author Plinio
 */
public class ControleAcessorio {
    
    ConectaBanco conectaBanco = new ConectaBanco();
    ModeloAcessorio modeloAcessorio = new ModeloAcessorio();
    
    public ControleAcessorio(){
        
    }
    
    public void salvar(ModeloAcessorio acessorio){
        conectaBanco.conexao();
        try {
           PreparedStatement stmt = conectaBanco.conectar.prepareStatement("insert into acessorios(codigo_acessorio,descricao_acessorio) values(?,?)");
           stmt.setString(1, acessorio.getCodigo());
           stmt.setString(2, acessorio.getDescricao()); 
           stmt.execute();
           stmt.close();
            JOptionPane.showMessageDialog(null, "Acessório cadastrado com sucesso!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao cadastrar acessório.\nErro: " + ex.getMessage());
        } 
        conectaBanco.desconecta();
    }
    
    public void consulta(ModeloAcessorio acessorio){
        String sql = "SELECT * FROM acessorio WHERE codigo = ?";
        
        try {
            PreparedStatement stmt;
            stmt = conectaBanco.conectar.prepareStatement(sql);
            stmt.setString(1, acessorio.getCodigo());
            //Recebe o resultado da pesquisa SQL
            stmt.execute();
            
            //Recupera o modelo da tabela usado na pesquisa de acessórios
            DefaultTableModel tableModel = (DefaultTableModel) FrmConsultaAcessorio.jTable1.getModel();
            //seta 0 para o numero de linhas da table
            tableModel.setNumRows(0);
            
            while(conectaBanco.rs.next()){
                tableModel.addRow(
                        new Object[]{
                            conectaBanco.rs.getString("codigo"),
                            conectaBanco.rs.getString("descricao")
                        }
                );
            }
            conectaBanco.rs.close();
            stmt.close();
            
       } catch (SQLException ex) {
            System.out.println("Erro ao executar a consulta SQL " + ex.getMessage());
        }
        
    }

    public void excluir(ModeloAcessorio modeloAcessorio) {
        conectaBanco.conexao();
        try {
            PreparedStatement pst = conectaBanco.conectar.prepareStatement("delete from acessorios where codigo_acessorio = ?");
            pst.setString(1, modeloAcessorio.getCodigo());
            pst.execute();
            JOptionPane.showMessageDialog(null, "Registro excluído com sucesso!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao excluir registro.\nErro: " + ex.getMessage());
        }
    }

    public void alterar(ModeloAcessorio modeloAcessorio) {
        conectaBanco.conexao();
        try {
            PreparedStatement pst = conectaBanco.conectar.prepareStatement("update acessorio set codigo_acessorio = ?, descricao_acessorio = ? where id_acessorio = ?");
            pst.setString(1, modeloAcessorio.getCodigo());
            pst.setString(2, modeloAcessorio.getDescricao());
            pst.setLong(3, modeloAcessorio.getId());
            pst.execute();
            JOptionPane.showMessageDialog(null, "Registro alterado com Sucesso!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao alterar dados.\nErro: " + ex.getMessage());
        }
        conectaBanco.desconecta();
    }

    public ModeloAcessorio primeiro() {
        conectaBanco.conexao();
        try {
            conectaBanco.executaSql("select * from acessorios");
            conectaBanco.rs.first();
            modeloAcessorio.setId(conectaBanco.rs.getLong("id_acessorio"));
            modeloAcessorio.setCodigo(conectaBanco.rs.getString("codigo_acessorio"));
            modeloAcessorio.setDescricao(conectaBanco.rs.getString("descricao_acessorio"));
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao preencher campos.\nErro: " + ex.getMessage());
        }
        conectaBanco.desconecta();
        return modeloAcessorio;
    }

    public ModeloAcessorio anterior() {
         conectaBanco.conexao();
        try {
            conectaBanco.rs.previous();
            modeloAcessorio.setId(conectaBanco.rs.getLong("id_acessorio"));
            modeloAcessorio.setCodigo(conectaBanco.rs.getString("codigo_acessorio"));
            modeloAcessorio.setDescricao(conectaBanco.rs.getString("descricao_acessorio"));
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao preencher campos.\nErro: " + ex.getMessage());
        }
        conectaBanco.desconecta();
        return modeloAcessorio;
    }

    public ModeloAcessorio proximo() {
         conectaBanco.conexao();
        try {
            conectaBanco.rs.next();
            modeloAcessorio.setId(conectaBanco.rs.getLong("id_acessorio"));
            modeloAcessorio.setCodigo(conectaBanco.rs.getString("codigo_acessorio"));
            modeloAcessorio.setDescricao(conectaBanco.rs.getString("descricao_acessorio"));
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao preencher campos.\nErro: " + ex.getMessage());
        }
        conectaBanco.desconecta();
        return modeloAcessorio;
    }

    public ModeloAcessorio ultimo() {
         conectaBanco.conexao();
        try {
            conectaBanco.executaSql("select * from acessorios");
            conectaBanco.rs.last();
            modeloAcessorio.setId(conectaBanco.rs.getLong("id_acessorio"));
            modeloAcessorio.setCodigo(conectaBanco.rs.getString("codigo_acessorio"));
            modeloAcessorio.setDescricao(conectaBanco.rs.getString("descricao_acessorio"));
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao preencher campos.\nErro: " + ex.getMessage());
        }
        conectaBanco.desconecta();
        return modeloAcessorio;
    }
    
}
