/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;

import factory.ConectaBanco;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import modelo.ModeloCliente;

/**
 *
 * @author Plinio
 */
public class ControleCliente {
    
    ConectaBanco conectaBanco = new ConectaBanco();
    long id;
    String nome;
    String endereco;
    String telefone;
    String cidade;
    
    public ControleCliente(){
        
    }
    
    public void adiciona(ModeloCliente cliente){
       String sql = "INSERT INTO cliente (nome,endereco,telefone,cidade) VALUES(?,?,?,?)";
        
        try {
            PreparedStatement stmt;
            stmt = conectaBanco.conectar.prepareStatement(sql);
            stmt.setString(1, cliente.getNome());
            stmt.setString(2, cliente.getEndereco());
            stmt.setString(3, cliente.getTelefone());
            stmt.setString(4, cliente.getCidade());
            stmt.execute();
            stmt.close();
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao executar consulta SQL " + ex.getMessage());
        } 
        conectaBanco.desconecta();
    }
}
