/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;

import factory.ConectaBanco;
import visao.FrmConsultaVidro;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import modelo.ModeloVidro;

/**
 *
 * @author Plinio
 */
public class ControleVidro {
    
    ConectaBanco conectaBanco = new ConectaBanco();
    ModeloVidro modeloVidro = new ModeloVidro();
    Long id;
    String cor;
    int espessura;
    
    public ControleVidro(){
        
    }
    
    public void salvar(ModeloVidro modeloVidro) {
        conectaBanco.conexao();
        
        try {
            PreparedStatement pst = conectaBanco.conectar.prepareStatement("insert into vidros (cor_vidro, espessura_vidro) values(?,?)");
            pst.setString(1, modeloVidro.getCor());
            pst.setInt(2, modeloVidro.getEspessura());
            pst.execute();
            pst.close();
            JOptionPane.showMessageDialog(null, "Dados inseridos com Sucesso!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao inserir dados.\nErro: " + ex.getMessage());
        }
        conectaBanco.desconecta();
        
    }

    public void alterar(ModeloVidro modeloVidro) {
        conectaBanco.conexao();
        try {
            PreparedStatement pst = conectaBanco.conectar.prepareStatement("update vidros set cor_vidro = ?, espessura_vidro = ? where id_vidro = ?");
            pst.setString(1, modeloVidro.getCor());
            pst.setInt(2, modeloVidro.getEspessura());
            pst.setLong(3, modeloVidro.getId());
            pst.execute();
            pst.close();
            JOptionPane.showMessageDialog(null, "Dados alterados com Sucesso!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao alterar dados.\nErro: " + ex.getMessage());
        }
    }

    public void excluir(ModeloVidro modeloVidro) {
        conectaBanco.conexao();
        
        try {
            PreparedStatement pst = conectaBanco.conectar.prepareStatement("delete from vidros where cor_vidro = ?");
            pst.setString(1, modeloVidro.getCor());
            pst.execute();
            pst.close();
            JOptionPane.showMessageDialog(null, "Registro excluído com sucesso");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao excluir registro.\nErro: " + ex.getMessage());
        }
        conectaBanco.desconecta();
    }
    
    public void consulta(ModeloVidro vidro){
        String sql = "SELECT * FROM vidro WHERE cor = ?";
        try {
            PreparedStatement stmt;
            stmt = conectaBanco.conectar.prepareStatement(sql);
            stmt.setString(1, vidro.getCor());
            
            //Recupera a pesquisa SQL do banco
            stmt.execute();
            //Recupera o modelo da tabela usado em ConsultaVidroGUI
            DefaultTableModel tableModel = (DefaultTableModel) FrmConsultaVidro.jTable1.getModel();
            
            //Seta o numero de linhas da tabela para 0
            tableModel.setNumRows(0);
            
            while(conectaBanco.rs.next()){
                tableModel.addRow(
                        new Object[]{
                            conectaBanco.rs.getString("cor"),
                            conectaBanco.rs.getString("espessura")
                        });
            }
            conectaBanco.rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.out.println("Erro ao executar consulta SQL " + ex.getMessage());
        }
        
    }

    public ModeloVidro primeiro() {
        conectaBanco.conexao();
        
        try {
            conectaBanco.executaSql("select * from vidros");
            conectaBanco.rs.first();
            modeloVidro.setId(conectaBanco.rs.getLong("id_vidro"));
            modeloVidro.setCor(conectaBanco.rs.getString("cor_vidro"));
            modeloVidro.setEspessura(conectaBanco.rs.getInt("espessura_vidro"));
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao preencher campos.\nErro: " + ex.getMessage());
        }
        conectaBanco.desconecta();
        return modeloVidro;
    }

    public ModeloVidro anterior() {
        conectaBanco.conexao();
        try {
            conectaBanco.rs.previous();
            modeloVidro.setId(conectaBanco.rs.getLong("id_vidro"));
            modeloVidro.setCor(conectaBanco.rs.getString("cor_vidro"));
            modeloVidro.setEspessura(conectaBanco.rs.getInt("espessura_vidro"));
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao preencher dados.\nErro: " + ex.getMessage());
        }
        conectaBanco.desconecta();
        return modeloVidro;
    }

    public ModeloVidro proximo() {
        conectaBanco.conexao();
        try {
            conectaBanco.rs.next();
            modeloVidro.setId(conectaBanco.rs.getLong("id_vidro"));
            modeloVidro.setCor(conectaBanco.rs.getString("cor_vidro"));
            modeloVidro.setEspessura(conectaBanco.rs.getInt("espessura_vidro"));
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao preencher dados.\nErro: " + ex.getMessage());
        }
        conectaBanco.desconecta();
        return modeloVidro;
    }

    public ModeloVidro ultimo() {
        conectaBanco.conexao();
        try {
            conectaBanco.executaSql("select * from vidros");
            conectaBanco.rs.last();
            modeloVidro.setId(conectaBanco.rs.getLong("id_vidro"));
            modeloVidro.setCor(conectaBanco.rs.getString("cor_vidro"));
            modeloVidro.setEspessura(conectaBanco.rs.getInt("espessura_vidro"));
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao preencher dados.\nErro: " + ex.getMessage());
        }
        conectaBanco.desconecta();
        return modeloVidro;
    }
}
