/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;


import factory.ConectaBanco;
import visao.FrmOrcamento;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import modelo.ModeloProduto;

/**
 *
 * @author Plinio
 */
public class ControleProduto {
    ConectaBanco conectaBanco = new ConectaBanco();
    String codigo;
    String descricao;
    
    public ControleProduto(){
        
    }
    
    public void consulta(ModeloProduto produto){
        String sql = "SELECT * FROM produto WHERE codigo = ?";
        
        try {
            PreparedStatement stmt = conectaBanco.conectar.prepareStatement(sql);
            stmt.setString(1, produto.getCodigo());
            stmt.execute();
           
            //Recupera modelo da tabela em OrcamentoGUI
            DefaultTableModel tableModel = (DefaultTableModel) FrmOrcamento.jTableBuscaProduto.getModel();
            
            //Define o numero de linhas da tabela para 0
            tableModel.setNumRows(0);
            
            
            while(conectaBanco.rs.next()){
                tableModel.addRow(new Object[]{
                    conectaBanco.rs.getInt("id"),
                    conectaBanco.rs.getString("codigo"),
                    conectaBanco.rs.getString("descricao"),
                    FrmOrcamento.txtLargura.getText(),
                    FrmOrcamento.txtAltura.getText(),
                    FrmOrcamento.txtQtde.getText()
                    
                });
            }
            conectaBanco.rs.close();
            stmt.close();
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao executar consulta SQL " + ex.getMessage());
        }
        
    }
    
}
