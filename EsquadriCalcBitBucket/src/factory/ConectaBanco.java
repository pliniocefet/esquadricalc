/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factory;

import java.sql.*;
import javax.swing.JOptionPane;

/**
 *
 * @author Plinio
 */
public class ConectaBanco {
    
    public Statement stm;
    public ResultSet rs;
    private String driver = "org.postgresql.Driver";
    private String caminho = "jdbc:postgresql://localhost:5432/esquadricalc";
    private String usuario = "postgres";
    private String senha = "root";
    public Connection conectar;
    
    public void conexao(){
        
        try {
            System.setProperty("jdbc.Driver", driver);
            conectar = DriverManager.getConnection(caminho, usuario, senha);
            //JOptionPane.showMessageDialog(null, "Conectado com sucesso!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Falha na conexão.\n Erro:" + ex.getMessage());
        }
    }
    
    public void executaSql(String sql){
        try {
            stm = conectar.createStatement(rs.TYPE_SCROLL_INSENSITIVE, rs.CONCUR_READ_ONLY);
            rs = stm.executeQuery(sql);
        } catch (SQLException ex) {
            //JOptionPane.showMessageDialog(null, "Erro no metodo ExecutaSql().\n Erro: " + ex.getMessage());
        }
    }
    
    public void desconecta(){
        
        try {
            conectar.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Falha ao fechar a conexão.\n Erro: " + ex.getMessage());
            
        }
    }
}


