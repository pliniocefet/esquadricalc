/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Plinio
 */
public class ModeloVidro {
    
    private Long id;
    private String cor;
    private int espessura;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the cor
     */
    public String getCor() {
        return cor;
    }

    /**
     * @param cor the cor to set
     */
    public void setCor(String cor) {
        this.cor = cor;
    }

    /**
     * @return the espessura
     */
    public int getEspessura() {
        return espessura;
    }

    /**
     * @param espessura the espessura to set
     */
    public void setEspessura(int espessura) {
        this.espessura = espessura;
    }
    
    
}
