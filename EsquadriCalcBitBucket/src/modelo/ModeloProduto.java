/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Plinio
 */
public class ModeloProduto {
    
    public Long id;
    public String codigo;
    public String descricao;
    public ModeloPerfil perfil;
    public ModeloAcessorio acessorio;
    public ModeloVidro vidro;
    public String linhaProduto;
    

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * @param descricao the descricao to set
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * @return the perfil
     */
    public ModeloPerfil getPerfil() {
        return perfil;
    }

    /**
     * @param perfil the perfil to set
     */
    public void setPerfil(ModeloPerfil perfil) {
        this.perfil = perfil;
    }

    /**
     * @return the acessorio
     */
    public ModeloAcessorio getAcessorio() {
        return acessorio;
    }

    /**
     * @param acessorio the acessorio to set
     */
    public void setAcessorio(ModeloAcessorio acessorio) {
        this.acessorio = acessorio;
    }

    /**
     * @return the vidro
     */
    public ModeloVidro getVidro() {
        return vidro;
    }

    /**
     * @param vidro the vidro to set
     */
    public void setVidro(ModeloVidro vidro) {
        this.vidro = vidro;
    }

    /**
     * @return the linhaProduto
     */
    public String getLinhaProduto() {
        return linhaProduto;
    }

    /**
     * @param linhaProduto the linhaProduto to set
     */
    public void setLinhaProduto(String linhaProduto) {
        this.linhaProduto = linhaProduto;
    }
    
}
